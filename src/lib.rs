
/* The makefile structure */
pub mod comment;

/* The makefile structure */
pub mod util;

pub use comment::singleline::SingleLine;

pub use comment::multiline::MultiLine;

