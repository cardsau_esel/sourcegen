#[derive(Debug)]
pub struct MultiLine {
    start : String,
    line : String,
    end : String,
    text : String
}

impl MultiLine
{
    pub fn from( start : & str, line  : & str, end   : & str, text  : & str)
            -> MultiLine {
        MultiLine {
            start : String::from( start ),
            line : String::from( line ),
            end : String::from( end ),
            text : String::from( text )
        }
    }

    pub fn source( & self, indent : usize ) -> String {
        let mut source = String::from( "" );

        if self.text.is_empty() {
            return source;
        }

        let indent_str = crate::util::indent( indent );
        let split = self.text.split( '\n' );
        let mut start = & self.start;

        /* A comment with more than one line consists of:
         *
         * start text[ 0 ]
         * line text[ 1 ]
         * ...
         * line text[ n ]
         * end
         */
        for i in split {
            if i.len() == 0 {
                source.push_str( & format!( "{}{}\n", & indent_str, & start ) );
            }
            else {
                source.push_str( & format!( "{}{} {}\n", & indent_str, & start, & i ) );
            }
            start = & self.line;
        }
        source.push_str( & format!( "{}{}\n", & indent_str, & self.end ) );

        return source;
    }

    pub fn add( & mut self, text : String ) {
        self.text.push_str( & text );
    }
}

impl std::fmt::Display for MultiLine
{
    fn fmt( & self, f : & mut std::fmt::Formatter<'_> ) -> std::fmt::Result {
        write!( f, "{}", self.source( 0 ) )
    }
}

//------------------------------------------------------------------------------
#[cfg(test)]
mod tests
{
    use super::*;

    /**
     * Create a simple single line C/C++ doxygen comment with one level of
     * indentation.
     */
    #[test]
    fn multi_line_single() {
        let c1 = MultiLine::from(
            "/*",
            " *",
            "*/",
            "Single line of text"
        );

        assert_eq!(
            c1.source( 1 ),
            String::from( "    /* Single line of text\n    */\n" )
        );
    }

    /**
     * Create a simple single line C/C++ doxygen comment with one level of
     * indentation.
     */
    #[test]
    fn multi_line() {
        let c1 = MultiLine::from(
            "/*",
            " *",
            " */",
            "First line of text\nsecond line\n\nfourth line"
        );

        assert_eq!(
            c1.source( 1 ),
            String::from(
                concat!(
                    "    /* First line of text\n",
                    "     * second line\n",
                    "     *\n",
                    "     * fourth line\n",
                    "     */\n"
                )
            )
        );
    }
}

